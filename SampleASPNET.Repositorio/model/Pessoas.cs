﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleASPNET.Repositorio.model
{
  public class Pessoas
  {
    //Tabela do banco
    public int Id { get; set; }
    public string Nome { get; set; }
    public string SobreNome { get; set; }
    public string Email { get; set; }

  }
}