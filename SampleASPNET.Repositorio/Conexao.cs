﻿using SampleASPNET.Repositorio.model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SampleASPNET.Repositorio
{
  // Instalar entityframework - Para utilizar DbContext
  // Install-Package EntityFramework 
  public class Conexao : DbContext
  {
    // Base -> Apontar Nome da connectionString que está no Web.Config
    public Conexao():base("dbSample")  {  }

    // Mapear minha tabela com minha classe
    //DbSet<Pessoas> Pessoas { get; set; }

    public System.Data.Entity.DbSet<SampleASPNET.Repositorio.model.Pessoas> Pessoas { get; set; }
  }
}