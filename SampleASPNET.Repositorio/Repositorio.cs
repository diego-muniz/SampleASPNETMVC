﻿using SampleASPNET.Repositorio.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SampleASPNET.Repositorio
{
  // Repositorio CRUD -> GENERICO 
  public class Repositorio<T> : IDisposable where T : class
  {
    // Conexão
    public Conexao conn { get; set; }

    // Abrir a conexão
    public Repositorio()
    {
      // Instanciar Conexão
      conn = new Conexao();
    }

    //Fechar Conexão
    public void Dispose()
    {
      conn.Dispose();
      conn = null;
    }


    //Listar GENERICO
    public List<T> Listar()
    {
      // Conn -> Conexão 
      // Set<T> -> O Nome da classe
      try
      {
        return conn.Set<T>().ToList();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }

    }


    // Adicionar Generico
    public void Add(T registro)
    {
      // T Nome da classe -> Registro Objeto a ser adicionado
      try
      {
        conn.Entry(registro).State = System.Data.Entity.EntityState.Added;
        conn.SaveChanges();
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public T Buscar(int Id)
    {
      try
      {
        return conn.Set<T>().Find(Id);
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public T Buscar(Expression<Func<T, bool>> filtro)
    {
      try
      {
        return conn.Set<T>().Where(filtro).FirstOrDefault();
      }
      catch (Exception ex)
      {

        throw new Exception(ex.Message);
      }
    }




    public void Excluir(T registro)
    {
      try
      {
        conn.Entry(registro).State = System.Data.Entity.EntityState.Deleted;
        conn.SaveChanges();
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public void Editar(T registro)
    {
      try
      {
        conn.Entry(registro).State = System.Data.Entity.EntityState.Modified;
        conn.SaveChanges();
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }

    public void Excluir(int Id)
    {
      try
      {
        T registro = Buscar(Id);

        if (registro != null)
        {
          Excluir(registro);
        }
        else
        {
          throw new Exception("O registro informado não foi encontrado");
        }

      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }



  }
}