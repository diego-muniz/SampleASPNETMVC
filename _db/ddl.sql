Create database dbSampleMVC

go

USE dbSampleMVC

GO

CREATE TABLE Pessoa(
Id INT PRIMARY KEY IDENTITY(1,1),
Nome VARCHAR(45),
SobreNome VARCHAR(45),
Email VARCHAR(45)
)