﻿using SampleASPNET.Repositorio;
using SampleASPNET.Repositorio.model;
using SampleASPNET.Repositorio.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleASPNET.MVC.Controllers
{
  public class PessoaController : Controller
  {
    // GET: Pessoa
    [HttpGet]
    public ActionResult Listar()
    {
      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        // Objeto Listar
        var listaPessoa = pessoa.Listar();

        // View Listar com a lista de pessoa
        return View(listaPessoa);
      }

    }

    [HttpGet]
    public ActionResult Add()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Add(Pessoas p)
    {
      using(PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        // Pessoa Add
        pessoa.Add(p);
        // Apos add retorna para view Listar passando a Lista
        return View("Listar", pessoa.Listar());
      }
    }

    public ActionResult Excluir(int Id)
    {
      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        pessoa.Excluir(Id);
        // Apos add retorna para view Listar passando a Lista
        return View("Listar", pessoa.Listar());
      }
    }

    public ActionResult Detalhe(int Id)
    {
      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        var p = pessoa.Buscar(Id);
        // Apos add retorna para view Listar passando a Lista
        return View(p);
      }
    }

    public ActionResult Editar(int Id)
    {
      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        var p = pessoa.Buscar(Id);
        // Mostrar Pessoa clicado
        return View(p);
      }
    }

    [HttpPost]
    public ActionResult Editar(Pessoas p)
    {
      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        pessoa.Editar(p);
        // Apos add retorna para view Listar passando a Lista
        return View("Listar", pessoa.Listar());

      }
    }


    public ActionResult ListarApi()
    {

      using (PessoasRepositorio pessoa = new PessoasRepositorio())
      {
        var listaPessoa = pessoa.Listar();


        return Json(new { clientes = listaPessoa }, JsonRequestBehavior.AllowGet);
      }

    }


  }
}